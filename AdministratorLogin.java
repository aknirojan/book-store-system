import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;

public class AdministratorLogin {

	private JFrame frame;
	private JTextField tbAdLogin;
	private JPasswordField passAdLogin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdministratorLogin window = new AdministratorLogin();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdministratorLogin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblLibrarianLoginPage = new JLabel("Administrator Login Page");
		lblLibrarianLoginPage.setBounds(6, 16, 180, 24);
		frame.getContentPane().add(lblLibrarianLoginPage);
		
		tbAdLogin = new JTextField();
		tbAdLogin.setBounds(149, 74, 130, 26);
		frame.getContentPane().add(tbAdLogin);
		tbAdLogin.setColumns(10);
		
		JButton btnAdLogin = new JButton("Login");
		btnAdLogin.setBounds(149, 199, 117, 29);
		frame.getContentPane().add(btnAdLogin);
		
		JLabel lblNewLabel = new JLabel("User Name:");
		lblNewLabel.setBounds(41, 79, 79, 16);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Password:");
		lblNewLabel_1.setBounds(41, 136, 96, 16);
		frame.getContentPane().add(lblNewLabel_1);
		
		passAdLogin = new JPasswordField();
		passAdLogin.setBounds(149, 131, 130, 26);
		frame.getContentPane().add(passAdLogin);
	}

}
